# react-resizer

A simple React library that allows for resizing of inner wrapper. This is just a test.

See the demo here: https://bitbucket.org/georgecz/react-resizer/src/master/demo/demo.mov

## Installation
Install using (if this actually gets published to NPM :))

`npm install react-resizer`

## Usage

```js
import React, {Component} from 'react'
import {render} from 'react-dom'

import ReactResizer from 'react-resizer'

class Demo extends Component {

  render() {
    return
        <ReactResizer
          borderSize={25}
          handleColor='black'
          backgroundColor='rgba(0,0,0, 0.7)'
          onDragStart={ () => console.log("Dragging started")}
          onDragEnd={ (size) => console.log("New size: " + size)}
        >
          <div style={{
              width: "30vw",
              height: '100vh',
              padding: 20
            }}>
            <span>This is the inner content!</span>
          </div>
        </ReactResizer>
  }
}

render(<Demo/>, document.querySelector('#demo'))
```

## Props

### `borderSize: number`
Size of the handle in px

### `backgroundColor: string`
Background color of the backdrop

### `handleColor: string`
Color of the handle

### `direction: 'horizontal' | 'vertical'`
Changes left/right or top to bottom

### `handlePosition: 'right' | 'left'`
If horizontal, where should be the handle rendered. This allows for putting the resizer on right side of the screen (see `/demo/src/index.js`).

### `onDragStart: () => void`
Callback when user starts dragging

### `onDrag: () => void`
Callback when user drags

### `onDragEnd: (newSize) => void`
Callback when user finishes dragging the handle.

## Contribution

Follow these steps to get the local development up and running.

- Clone the repository
- `npm install`
- `npm start`

You should see the prompt informing you about the server running. Open your web browser at http://localhost:3000 and you should see the demo page.
