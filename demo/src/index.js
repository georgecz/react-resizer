import React, {Component} from 'react'
import {render} from 'react-dom'

import ReactResizer from '../../src'

class Demo extends Component {

  state = {vertical: false, onRight: false}

  _toggleState = (key) => {
    this.setState(state =>
      { return { [key]: !state[key] }
    });
  }

  render() {
    return <div style={{
      display: 'flex',
      justifyContent: this.state.onRight ? 'flex-end' : 'flex-start'
    }}>
        <ReactResizer
          borderSize={25}
          handleColor='black'
          backgroundColor='rgba(0,0,0, 0.7)'
          direction={this.state.vertical ? "vertical" : "horizontal"}
          handlePosition={this.state.onRight ? 'left' : 'right'}
          onDragStart={ () => console.log("Dragging started")}
          onDragEnd={ (size) => console.log("Dragging ended, new size: " + size)}
        >
          <div style={{
              width: this.state.vertical ? "100vw" : "30vw",
              height: this.state.vertical ? '30vh' : '100vh',
              padding: 20
            }}>
            <span>Ahoj!</span>
            <button onClick={() => this._toggleState('vertical')}>Toggle direction!</button>
            <button onClick={() => this._toggleState('onRight')}>Toggle side!</button>
          </div>
        </ReactResizer>
      </div>
  }
}

render(<Demo/>, document.querySelector('#demo'))
