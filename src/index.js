import React, {Component} from 'react'
import PropTypes from 'prop-types';

import { DimensionProvider } from './DimensionPovider';

const Handle = ({onMouseDown, size, color, axis, invertedAxis, position}) => {
  const style = {
    [axis]: size,
    backgroundColor: color,
    position: 'absolute',
    [position]: 0,
    [invertedAxis]: '100%'
  }

  axis === "horizontal" ? style.top = 0 : style.bottom = 0

  return <div style={style}
    onMouseDown={onMouseDown}>
  </div>
}

const styles = {
  wrapper: {
    display: 'inline-block',
    overflow: 'hidden',
    position: 'relative'
  }
}
export default class Resizer extends Component {

  constructor(props) {
    super();
    this.d = new DimensionProvider(props.direction)
    this.state = {
      isDragging: false,
      delta: 0
    }
  }

  componentWillReceiveProps(newProps) {
    if (newProps.direction !== this.props.direction) {
      this.d.direction = newProps.direction;
      this._onWindowResize();
    }
  }

  _onMouseDown = (e) => {
    e.preventDefault();
    this.setState({
      isDragging: true,
      start: e[this.d.clientCoord]
    });

    if (typeof this.props.onDragStart === 'function') {
        this.props.onDragStart(this.state.size);
      }
  }

  _onMouseUp = (e) => {
    e.preventDefault();
    this.setState({
      isDragging: false,
      start: 0,
      delta: 0,
      size: this.calculateRenderedSize()
    }, () => {
      if (typeof this.props.onDragEnd === 'function') {
        this.props.onDragEnd(this.state.size);
      }
    });
  }

  _onMouseMove = (e) => {
    e.preventDefault();
    if(this.state.isDragging) {
      this.setState({
        delta: this.state.start - e[this.d.clientCoord]
      }, () => {
        if (typeof this.props.onDrag === 'function') {
          this.props.onDrag();
        }
      });
    }

  }

  _onWindowResize = () => {
    this.setState({
      maxSize: document.documentElement[this.d.clientSize]
    })
  }

  componentDidMount() {
    document.addEventListener("mousemove", this._onMouseMove);
    document.addEventListener("mouseup", this._onMouseUp);
    window.addEventListener("resize", this._onWindowResize);
    this._onWindowResize();
  }
  componentWillUnmount() {
    document.removeEventListener("mousemove", this._onMouseMove);
    document.removeEventListener("mouseup", this._onMouseUp);
    window.removeEventListener("resize", this._onWindowResize)
  }

  elementRef = (el) => {
    el && this.setState({
      size: el[this.d.clientSize]
    })
  }

  componentDidUpdate(_, prevState) {
    if (prevState.maxSize !== this.state.maxSize) {
      this.setState({
        size: Math.min(this.state.maxSize, Math.max(this.state.size, 0))
      });
    }
  }

  calculateRenderedSize() {
    let { delta } = this.state;
    let { handlePosition, direction } = this.props;
    if (handlePosition === 'left' && direction === 'horizontal') delta = -delta;

    return this.state.size !== undefined
      ? Math.min(this.state.maxSize, Math.max(this.state.size - delta, 0))
      : undefined
  }

  render() {
    return <div
      style={{...styles.wrapper,
        minWidth: this.props.borderSize,
        minHeight: this.props.borderSize,
        [this.d.length]: this.calculateRenderedSize(),
        backgroundColor: this.props.backgroundColor
      }}
      ref={ this.elementRef }>
        <Handle
          onMouseDown={ this._onMouseDown }
          size={ this.props.borderSize }
          color={ this.props.handleColor }
          axis={ this.d.length }
          invertedAxis={ this.d.invertedLength }
          position={ this.props.handlePosition }
        />
        { this.props.children }
    </div>
  }
}

Resizer.defaultProps = {
  borderSize: 20,
  backgroundColor: 'rgba(0, 0, 0, 0.5)',
  handleColor: 'rgba(0,0,0,0.8)',
  direction: 'horizontal',
  handlePosition: 'right'
}

Resizer.propTypes = {
  borderSize: PropTypes.number,
  backgroundColor: PropTypes.string,
  handleColor: PropTypes.string,
  direction: PropTypes.oneOf(['horizontal', 'vertical']),
  handlePosition: PropTypes.oneOf(['right', 'left']),
  onDragStart: PropTypes.func,
  onDrag: PropTypes.func,
  onDragEnd: PropTypes.func,
}

