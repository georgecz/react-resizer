const horizontal = "horizontal";

export class DimensionProvider {
    constructor(direction){
        this._direction = direction;
    }
    set direction(d) {
        this._direction = d
    }
    get clientCoord () {
        return this._direction === horizontal ? "clientX" : "clientY"
    }
    get clientSize () {
        return this._direction === horizontal ? "clientWidth" : "clientHeight"
    }
    get invertedLength () {
        return this._direction === horizontal ? "height" : "width"
    }
    get length () {
        return this._direction === horizontal ? "width" : "height"
    }
}
